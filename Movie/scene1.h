#ifndef SCENE1_INCLUDED__
#define SCENE1_INCLUDED__

class Scene1{
	static const int swordNum{ 5 };
	int back;
	int start;
	
	int sword[swordNum];
	int sNum{ 0 };
	int sPosX;
	int sPosY;
	void Draw();
public:
	Scene1();
	~Scene1();
	bool end{ false };
	void Init();
	void Update(int counter);
};

#endif