#include "Dxlib.h"
#include "movieMain.h"
#include "fps.h"

void gameMain(){
	// 描画先をバックスクリーンに設定する
	SetDrawScreen(DX_SCREEN_BACK);

	/* 初期化 */
	Fps f;
	Init_Movie();

	// メインループ
	while (ProcessMessage() != -1){
		// 画面をクリアする
		ClearDrawScreen();

		/* Update */
		f.Update();
		Update_Movie();
		Draw_Movie();

		// バックスクリーンとフロントスクリーンを交換する
		ScreenFlip();
		f.Wait();
	}
}



//##########################################################################################################
//################   ↓画面モード(ウィンドウ・フル・大きさ)を変える以外は触れないと思う↓   ################
//##########################################################################################################
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
	// ウィンドウモードに設定
	ChangeWindowMode(true);
	SetGraphMode(1280, 960, 16);

	// DXライブラリ初期化
	if (DxLib_Init() == -1){
		return -1;
	}
	gameMain();

	DxLib_End();
	return 0;
}