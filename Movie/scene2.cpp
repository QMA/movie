//#include "DxLib.h"
//#include <string>
//#include "scene2.h"
//#include "effect.h"
//
//Mushroom *mush;
//int death[7];
//
//Scene2::Scene2(){
//	back = LoadGraph("img/scene2/back.png");
//	mark = LoadGraph("img/scene2/exclumation.png");
//	for (int i = 0; i < manNum; i++){
//		man[i] = LoadGraph(("img/scene2/man/" + std::to_string(i) + ".png").c_str());
//	}
//	for (int i = 0; i < 5; i++){
//		sword[i] = LoadGraph(("img/scene2/sword/" + std::to_string(i) + ".png").c_str());
//	}
//	for (int i = 0; i < 10; i++){
//		game[i] = LoadGraph(("img/scene2/game/" + std::to_string(i) + ".png").c_str());
//	}
//	LoadDivGraph("img/scene2/bom.png", 7, 7, 1, 450, 450, death, 0);
//	cplus = LoadGraph("img/scene2/cplus.png");
//	java = LoadGraph("img/scene2/java.png");
//
//	e2 = new Effect2;
//	e2->useIn = true;
//
//	mPosX = 200;
//	mPosY = 450;
//	mNum = 1;
//
//	sPosX = 330;
//	sPosY = 320;
//
//	//最初に画面を暗くする
//	//SetDrawBright(0, 0, 0);
//
//	mush = new Mushroom(1280, 500, 500);
//	mush->stop = true;
//
//	e3 = new Effect3;
//	e4 = new Effect4(death);
//}
//
//void Scene2::Update(int counter){
//	mush->Pattern();
//	if (counter < 160){
//		if (counter % 8 == 0){
//			mNum++;
//			if (mNum == 4) mNum = 1;
//		}
//	}
//	if (scene == 3){
//		if(turn == false) move += 40;
//		else move -= 40;
//		if (move > 640){
//			move = 640;
//		}
//		if (counter % 10 == 0){
//			gNum++;
//			if (gNum == 10) gNum = 9;
//		}
//	}
//	if (scene == 4 && mush->end == true){
//		e4->use = true;
//		scene = 5;
//	}
//
//	switch (counter){
//	case 120:
//		mush->stop = false;
//		break;
//	case 160:
//		scene = 1;
//		mNum = 0;
//		break;
//	case 224:
//		mNum = 4;
//		scene = 2;
//		break;
//	case 295:
//		mNum = 5;
//		sNum = 1;
//		sPosY = 600;
//		break;
//	case 301:
//		mNum = 6;
//		sNum = 2;
//		sPosX = 110;
//		sPosY = 560;
//		break;
//	case 344:
//		mNum = 7;
//		mPosX = 300;
//		sPosX = 210;
//		e3->use = true;
//		mush->stop = true;
//		break;
//	case 347:
//		mNum = 8;
//		sNum = 1;
//		mPosX = 600;
//		sPosX = 750;
//		break;
//	case 350:
//		mNum = 9;
//		mPosX = 800;
//		sPosX = 950;
//		scene = 3;
//		break;
//	case 510:
//		turn = true;
//		break;
//	case 540:
//		scene = 4;
//		mush->dead = true;
//		break;
//	case 605:
//		mNum = 10;
//		sNum = 3;
//		sPosX = 940;
//		sPosY = 640;
//		break;
//	case 611:
//		mNum = 0;
//		sNum = 4;
//		sPosX = 890;
//		sPosY = 670;
//		break;
//	case 670:
//		end = true;
//		break;
//	}
//
//	Draw();
//}
//
//void Scene2::Draw(){
//	if (e2->useIn == true) e2->fadeIn();
//
//	if (scene == 3) SetDrawBright(35, 35, 35);
//	DrawGraph(0, 0, back, TRUE);
//	if (scene >= 2) DrawGraph(sPosX, sPosY, sword[sNum], TRUE);
//	DrawGraph(mPosX, mPosY, man[mNum], TRUE);
//	if (scene == 1) DrawGraph(50, 180, mark, TRUE);
//
//	if (mush->end == false) mush->Draw(0);
//	if (e3->use == true) e3->Draw(300, 480);
//
//	if (scene == 3){
//		SetDrawBright(255, 255, 255);
//		DrawGraph(-590 + move, 100, cplus, TRUE);
//		DrawGraph(1280 - move, 20, java, TRUE);
//		DrawGraph(180, 420, game[gNum], TRUE);
//	}
//	if (e4->use == true){
//		e4->Draw(300, 380);
//	}
//}
//
//Scene2::~Scene2(){
//	delete e2;
//	delete e3;
//	delete e4;
//}
//
//Mushroom::Mushroom(int x, int y, int firstY){
//	this->x = x;
//	this->y = y;
//	this->firstY = firstY;
//	for (int i = 0; i < 3; i++){
//		picture[i] = LoadGraph(("img/scene2/mushroom/" + std::to_string(i) + ".png").c_str());
//	}
//}
//
//void Mushroom::Pattern(){
//	if (stop == false){
//		x -= 4;
//		time += 0.22;
//
//		y = firstY - (int)(sqrt(2.000 * 9.8 * 50) * time - 0.500 * 9.8 * time * time);
//		if (y > firstY){
//			time = 0.0;
//		}
//	}
//	if (dead == true){
//		difference +=2;
//		if (difference > 100){
//			end = true;
//		}
//	}
//}
//
//void Mushroom::Draw(int screenX){
//	if (dead == false){
//		if (time > 2){
//			DrawGraph(x - screenX, y, picture[1], true);
//		}
//		else{
//			DrawGraph(x - screenX, y, picture[0], true);
//		}
//	}
//	else {
//		DrawRectGraph(x - screenX + difference, y, 0, 0, 290, 147, picture[1], true, false);
//		DrawRectGraph(x - screenX - difference, y + 147, 0, 147, 290, 167, picture[1], true, false);
//	}
//}