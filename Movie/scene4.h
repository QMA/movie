#ifndef SCENE4_INCLUDED__
#define SCENE4_INCLUDED__
#include "effect.h"
#include <cstdint>


class Box{
public:
	int_fast16_t time{ 0 };// 7���ȓ�
	int_fast16_t x{ 0 };
	int_fast16_t y{ 0 };
	int_fast16_t dx{ 0 };
	int_fast16_t dy{ 0 };
	bool start{ false };
	bool fell{ false };
	static const int_fast8_t numW{ 20 };
	static const int_fast8_t numH{ 10 };
	Box(int_fast16_t x, int_fast16_t y, int_fast16_t dx, int_fast16_t dy);
};

class Scene4{
	int_fast8_t scene{ 0 };
	int back;

	int boxSide;
	int lengthW;
	int lengthH;
	int boxTop;
	bool showTop[Box::numW];
	int_fast16_t topY{ 0 };

	int secret;
	int button;
	int door;
	int doorEdge;
	int exitDoor;
	int_fast16_t dX{ 0 };

	int_fast16_t scrollX{ 0 };
	int_fast16_t scrollY{ 0 };
	int_fast16_t time{ 0 };

	int picture[3][Box::numW*Box::numH];
	int_fast8_t pictureNum{ 0 };
	
	static const int_fast8_t manNum{ 6 };
	int man[manNum];
	int_fast8_t mNum{ 0 };
	int_fast16_t mPosX{ 0 };
	int_fast16_t mPosY{ 0 };

	Box* box[Box::numW][Box::numH];
	Effect2* e2;
	void Draw();
	bool Fall(int_fast16_t baseLine);
	void ReadyForFall();
public:
	Scene4();
	~Scene4();
	bool end{false};
	void Update(int counter);
	void Start(){ e2->useIn = true; }
};

bool Fall(int_fast16_t baseLine);
void ReadyForFall();
#endif