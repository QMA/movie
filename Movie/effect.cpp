#include "effect.h"
#include "DxLib.h"
#include <string>

Effect0::Effect0(){
	for (int i = 0; i < pictureNum; i++)
		picture[i] = LoadGraph(("img/scene1/effect0/" + std::to_string(i) + ".png").c_str());
}

void Effect0::Draw(int x, int y){
	Update();
	DrawGraph(x, y, picture[showNum], TRUE);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
}

void Effect0::Update(){
	count++;
	switch (count){
	case 2:
		showNum++;
		break;
	case 3:
		showNum++;
		break;
	case 5:
		showNum++;
		break;
	}

	if (count > 6){
		if (count % 7 ==	 0){
			showNum++;
			if (showNum == pictureNum) showNum = 3;
		}
		alpha -= 10;
		if (alpha < 0){
			alpha = 0;
			use = false;
		}
	}
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
}

int Effect1::Move(const int value){
	if (use == true){
		if(count % 3 == 0)move++;
		if(value < count) {
			use = false;
		}
		count++;
	}
	return move;
}

// フェードイン処理
bool Effect2::fadeIn(){
	if (useIn == true){
		SetDrawBright(countIn, countIn, countIn);

		countIn += 8;
		if (countIn > 255){
			SetDrawBright(255, 255, 255);
			useIn = false;
			return true;
		}
	}

	return false;
}

// フェードアウト処理
bool Effect2::fadeOut(){
	if (useOut == true){
		SetDrawBright(255 - countOut, 255 - countOut, 255 - countOut);

		countOut += 8;
		if (countOut > 255){
			SetDrawBright(0, 0, 0);
			useOut = false;
			return true;
		}
	}

	return false;
}

// フェードアウト処理
bool Effect2::fadeOutSemi(){
	if (useOutSemi == true){
		SetDrawBright(255 - countOutSemi, 255 - countOutSemi, 255 - countOutSemi);

		countOutSemi += 20;
		if (countOutSemi > 180){
			SetDrawBright(75, 75, 75);
			useOutSemi = false;
			return true;
		}
	}

	return false;
}

Effect3::Effect3(){
	for (int i = 0; i < pictureNum; i++)
		picture[i] = LoadGraph(("img/scene2/effect3/" + std::to_string(i) + ".png").c_str());
}

void Effect3::Draw(int x, int y){
	Update();
	DrawGraph(x, y, picture[showNum], TRUE);
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
}

void Effect3::Update(){
	count++;
	switch (count){
	case 2:
		showNum++;
		break;
	case 3:
		showNum++;
		break;
	case 5:
		showNum++;
		break;
	}

	if (count > 6){
		if (count % 7 == 0){
			showNum++;
			if (showNum == pictureNum) showNum = 3;
		}
		alpha -= 10;
		if (alpha < 0){
			alpha = 0;
			use = false;
		}
	}
	SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
}

void Effect4::Draw(int x, int y){
	count++;
	if (count % 4 == 0){
		showNum++;
		if (showNum == 7){
			showNum = 4;
		}
		if (count > 74){
			use = false;
		}
	}
	DrawGraph(x, y, picture[showNum], TRUE);
}