#include "DxLib.h"
#include <string>
#include "scene2_b.h"
#include "effect.h"

Mushroom *mush;
int death[7];

Scene2::Scene2(){
	//movieHandle = LoadGraph("movie/test.wmv");
	//PlayMovieToGraph(movieHandle);
	//DrawRectGraph(450 + e1->Move(65), 670, 0, 0, 318, 72, start, TRUE, FALSE); DrawGraph(0, 0, movieHandle, FALSE);

	back = LoadGraph("img/scene2/back.png");
	mark = LoadGraph("img/scene2/exclumation.png");
	for (int i = 0; i < manNum; i++){
		man[i] = LoadGraph(("img/scene2/man/" + std::to_string(i) + ".png").c_str());
	}
	for (int i = 0; i < 5; i++){
		sword[i] = LoadGraph(("img/scene2/sword/" + std::to_string(i) + ".png").c_str());
	}
	LoadDivGraph("img/scene2/bom.png", 7, 7, 1, 450, 450, death, 0);
	
	//ムービーファイルをロード。
	movieHandle = LoadGraph("movie/test.wmv");
	binetHandle = LoadGraph("img/scene2/binet.png");
	crashHandle = LoadGraph("img/scene2/crash.png");
	dodaiHandle = LoadGraph("img/scene2/dodai.png");
	for (int i = 0; i < 2; i++){
		subjectHandle[i] = LoadGraph(("img/scene2/subject" + std::to_string(i) + ".png").c_str());
	}

	e2 = new Effect2;

	mPosX = -260;
	mPosY = 540;
	mNum = 1;

	sPosX = 270;
	sPosY = 410;

	tvX = 800;
	tvY = 30;

	//最初に画面を暗くする
	SetDrawBright(0, 0, 0);

	mush = new Mushroom(1280, 590, 590);
	mush->stop = true;

	e3 = new Effect3;
	e4 = new Effect4(death);
}

void Scene2::Update(int counter){
	const int_fast16_t add = 550;
	mush->Pattern();
	if (60 < counter && counter < 90){
		backX -= 7;
		tvX -= 7;
	}
	if (counter < 60){
		mPosX += 7;
	}
	if (counter < 160 || scene == 7){
		if (counter % 4 == 0){
			mNum++;
			if (mNum == 4) mNum = 1;
		}
	}
	if (90 <= counter && counter < 160){
		tvX -= 7;
	}
	if (counter == 160){
		mNum = 0;
	}

	if (scene == 5 && mush->end == true){
		e4->use = true;
		scene = 6;
	}

	subjectNum = counter/10%2;
	if (subjectNum == 2){
		subjectNum = 0;
	}

	if (scene == 7){
		if (backX > -320){
			backX -= 7;
			tvX -= 7;
		}
		else {
			mPosX += 7;
			if (mPosX > 1000){
				e2->useOut = true;
			}
		}
	}

	switch (counter){
	case 220:
		// ムービーを再生状態にします
		PlayMovieToGraph(movieHandle);
		scene = 1;
		mNum = 0;
		break;
	case 100 + add:
		scene = 2;
		break;
	case 220 + add:
		mush->stop = false;
		break;
	case 234 + add:
		scene = 3;
		break;
	case 325 + add:
		mNum = 4;
		scene = 4;
		break;
	case 345 + add:
		mNum = 5;
		sNum = 1;
		sPosY = 690;
		break;
	case 351 + add:
		mNum = 6;
		sNum = 2;
		sPosX = 50;
		sPosY = 650;
		break;
	case 394 + add:
		mNum = 7;
		mPosX = 300;
		sPosX = 210;
		e3->use = true;
		mush->stop = true;
		break;
	case 397 + add:
		mNum = 8;
		sNum = 1;
		mPosX = 600;
		sPosX = 750;
		break;
	case 400 + add:
		mNum = 9;
		mPosX = 800;
		sPosX = 950;
		break;
	case 410 + add:
		scene = 5;
		mush->dead = true;
		break;
	case 445 + add:
		mNum = 10;
		sNum = 3;
		sPosX = 940;
		sPosY = 730;
		break;
	case 451 + add:
		mNum = 0;
		sNum = 4;
		sPosX = 890;
		sPosY = 760;
		break;
	case 590 + add:
		scene = 7;
		break;
	}

	Draw();
}

void Scene2::Draw(){
	if (e2->useIn == true) e2->fadeIn();
	if (e2->useOut == true && e2->fadeOut()){
		end = true;
	}

	//背景
	DrawGraph(backX, 0, back, TRUE);
	//tv土台
	DrawGraph(tvX + 412, tvY + 768, dodaiHandle, TRUE);
	//tv枠線
	DrawBox(tvX, tvY, tvX + 1024, tvY - 20, GetColor(0, 0, 0), TRUE);
	DrawBox(tvX, tvY + 768, tvX + 1024, tvY + 788, GetColor(0, 0, 0), TRUE);
	DrawBox(tvX, tvY - 20, tvX - 20, tvY + 788, GetColor(0, 0, 0), TRUE);
	DrawBox(tvX + 1024, tvY - 20, tvX + 1044, tvY + 788, GetColor(0, 0, 0), TRUE);
	//ブラウン管時代を思い出す画像
	DrawGraph(tvX, tvY, binetHandle, FALSE);
	if(scene == 0)DrawGraph(tvX, tvY, subjectHandle[subjectNum], TRUE);
	//movie
	if (scene >= 1 && scene < 6){
		SetDrawBright(255, 255, 255);
		if (scene >= 2) SetDrawBright(165, 165, 165);
		DrawGraph(tvX, tvY, movieHandle, FALSE);
		SetDrawBright(35, 35, 35);
		if (scene >= 2) SetDrawBright(255, 255, 255);
	}
	if (scene >= 6){
		DrawGraph(tvX + 200, tvY + 300, crashHandle, TRUE);
	}
	
	//剣
	if (scene >= 4 && scene < 7){
		DrawGraph(sPosX, sPosY, sword[sNum], TRUE);
	}
	//主人公
	DrawGraph(mPosX, mPosY, man[mNum], TRUE);
	//マーク
	if (scene == 3){
		DrawGraph(50, 270, mark, TRUE);
	}

	if (mush->end == false) mush->Draw(0);
	if (e3->use == true) e3->Draw(300, 570);

	if (e4->use == true){
		e4->Draw(300, 380);
	}
	
}

Scene2::~Scene2(){
	delete e2;
	e2 = NULL;
	delete e3;
	e3 = NULL;
	delete e4;
	e4 = NULL;
	//InitGraph();
}

Mushroom::Mushroom(int x, int y, int firstY){
	this->x = x;
	this->y = y;
	this->firstY = firstY;
	for (int i = 0; i < 3; i++){
		picture[i] = LoadGraph(("img/scene2/mushroom/" + std::to_string(i) + ".png").c_str());
	}
}

void Mushroom::Pattern(){
	if (stop == false){
		x -= 5;
		time += 0.22;

		y = firstY - (int)(sqrt(2.000 * 9.8 * 50) * time - 0.500 * 9.8 * time * time);
		if (y > firstY){
			time = 0.0;
		}
	}
	if (dead == true){
		difference += 2;
		if (difference > 100){
			end = true;
		}
	}
}

void Mushroom::Draw(int screenX){
	if (dead == false){
		if (time > 2){
			DrawGraph(x - screenX, y, picture[1], true);
		}
		else{
			DrawGraph(x - screenX, y, picture[0], true);
		}
	}
	else {
		DrawRectGraph(x - screenX + difference, y, 0, 0, 290, 147, picture[1], true, false);
		DrawRectGraph(x - screenX - difference, y + 147, 0, 147, 290, 167, picture[1], true, false);
	}
}