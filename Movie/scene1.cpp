#include "DxLib.h"
#include <string>
#include "scene1.h"
#include "effect.h"

Effect0 *e0;
Effect1 *e1;
Effect2 *e2;

Scene1::Scene1(){
	back = LoadGraph("img/scene1/back.png");
	start = LoadGraph("img/scene1/start.png");
	for (int i = 0; i < swordNum; i++) sword[i] = LoadGraph(("img/scene1/sword" + std::to_string(i) + ".png").c_str());

	sPosY = 710;
	sPosX = 250;

	e0 = new Effect0;
	e1 = new Effect1;
	e2 = new Effect2;
	e2->useIn = true;
	

	//最初に画面を暗くする
	SetDrawBright(0, 0, 0);
}

void Scene1::Update(int counter){

	switch (counter){
	case 120:
		sPosY = 810;
		break;
	case 145:
		sPosY = 710;
		break;
	case 180:
		//sPos = 840;
		sNum++;
		break;
	case 184:
		sNum++;
		//sPos = 710;
		break;
	case 188:
		sNum++;
		//sPos = 840;
		break;
	case 192:
		sNum++;
		sPosY = 720;
		sPosX = 220;
		break;
	case 194:
		e0->use = true;
		sNum = 1;
		sPosX = 760;
		sPosY = 720;
		break;
	case 218:
		e1->use = true;
		break;
	case 230:
		e2->useOut = true;
		break;
	}

	Draw();
}

void Scene1::Draw(){
	if (e2->useIn == true) e2->fadeIn();
	if (e2->useOut == true && e2->fadeOut()){
		end = true;
	}

	DrawGraph(0, 0, back, TRUE);
	DrawRectGraph(450 + e1->Move(65), 670, 0, 0, 318, 72, start, TRUE, FALSE);
	DrawRectGraph(450 - e1->Move(65), 742, 0, 72, 318, 72, start, TRUE, FALSE);
	DrawGraph(sPosX, sPosY, sword[sNum], TRUE);
	if (e0->use == true) e0->Draw(430, 685);
}

Scene1::~Scene1(){
	delete e0;
	delete e1;
	delete e2;
	//InitGraph();
}