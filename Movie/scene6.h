#ifndef SCENE6_INCLUDED__
#define SCENE6_INCLUDED__
#include "effect.h"
#include <cstdint>

class Scene6{
	int_fast8_t scene{ 0 };

	int san;
	int back_u;
	int back_d;
	int sword;
	int black;
	int address;
	int syoriken;
	int jouhou;
	int END[3];

	int mark;
	float angle{ 0.0 };

	int white;
	int back_alpha;
	float alpha{ 0.0 };

	//人に関する変数
	static const int_fast8_t manNum{ 4 };
	int man[manNum];
	int_fast8_t mNum{ 0 };
	int_fast16_t mPosX{ 484 };
	int_fast16_t mPosY{ 960 };

	Effect2* e2;

	void Draw();
public:
	Scene6();
	~Scene6();
	bool end{ false };
	void Update(int counter);
	void Start(){ e2->useIn = true; }
};

#endif