#ifndef SCENE3_INCLUDED__
#define SCENE3_INCLUDED__

class Scene3{
	static const int swordNum{ 5 };
	static const int manNum{ 11 };
	int back;
	int start;


	int man[manNum];
	int mNum{ 0 };
	int mPosX{ 0 };
	int mPosY{ 0 };

	int sword[swordNum];
	int sNum{ 0 };
	int sPosX;
	int sPosY;
public:
	Scene3();
	~Scene3();
	bool end{ false };
	void Init();
	void Update(int counter);
	void Draw();
};

#endif