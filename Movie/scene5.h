#ifndef SCENE5_INCLUDED__
#define SCENE5_INCLUDED__
#include "effect.h"
#include <cstdint>

class Block{
public:
	Block(int_fast16_t x, int_fast16_t y, int_fast8_t num, int_fast8_t ix, int_fast16_t dx, bool start){
		this->x = x;
		this->y = y;
		this->num = num;
		this->ix = ix;
		this->dx = dx;
		this->start = start;
	};
	int_fast16_t x{ 0 };
	int_fast16_t y{ 0 };
	int_fast8_t num{ 0 };
	int_fast8_t ix{ 0 };
	int_fast16_t dx{ 0 };
	int_fast16_t time{ 0 };
	bool start{ false };
	bool fell{ false };
};

class Scene5{
	int_fast8_t scene{ 0 };
	int block[3];
	int piano;

	int door;
	int e_door;
	int_fast16_t doorX{ 0 };

	int exit[4];
	int key;
	int key_i[27];
	int s_small[8];
	int s_large[3];
	int speaker;

	int back;
	int scroll1{ 0 };//{ -960 };
	int scroll2{ 0 };

	//人に関する変数
	static const int_fast8_t manNum{ 8 };
	int man[manNum];
	int_fast8_t mNum{ 0 };
	uint_fast16_t mTime{ 0 };
	int_fast16_t mPosX{ 540 };
	int_fast16_t mPosY{ -250 };

	Effect2* e2;

	int_fast8_t bNum{ 0 };
	int_fast16_t scroll_b{ 0 };
	Block* b[31];

	void Draw();
	void ScrollBack(int count);
	void ManScroll(int counter);
	void Pattern1();//DTM文字列の配置
	void Pattern2();//ピアノロールの配置
	void Pattern3();//キー画像及び出口の配置
	bool toLeft(int_fast8_t startNum,int_fast8_t speed);
public:
	Scene5();
	~Scene5();
	bool end{ false };
	void Update(int counter);
	void Start(){ e2->useIn = true; }
};

#endif