#include"DxLib.h"
#include"scene5.h"
#include <string>

Scene5::Scene5(){
	back = LoadGraph("img/scene5/back1.png");
	piano = LoadGraph("img/scene5/piano.png");
	door = LoadGraph("img/scene5/door.png");
	e_door = LoadGraph("img/scene5/e_door.png");
	LoadDivGraph("img/scene5/exit.png", 4, 2, 2, 221, 245, exit);
	key = LoadGraph("img/scene5/key.png");
	LoadDivGraph("img/scene5/small.png", 8, 8, 1, 60, 90, s_small);
	LoadDivGraph("img/scene5/large.png", 3, 3, 1, 210, 190, s_large);
	LoadDivGraph("img/scene5/screen.png", 27, 9, 3, 132, 99, key_i);
	speaker = LoadGraph("img/scene5/box.png");
	block[0] = LoadGraph("img/scene5/block/g_small.png");
	block[1] = LoadGraph("img/scene5/block/g_large.png");
	block[2] = LoadGraph("img/scene5/block/green.png");
	for (int i = 0; i < manNum; i++){
		man[i] = LoadGraph(("img/scene5/man/" + std::to_string(i) + ".png").c_str());
	}
	
	//最初に画面を暗くする
	SetDrawBright(0, 0, 0);
	e2 = new Effect2;
	//e2->useIn = true;
	/*for (int i = 0; i < bNum; i++){
		if (i % 7 < 4) b[i] = new Block(80 + i % 7 * 307, 834 - i / 7 * 320);
		else b[i] = new Block(210 + (i % 7 - 4) * 357, 674 - i / 7 * 320);
	}*/
}

void Scene5::Update(int counter){
	if (scene != 2 && scene != 4 && scene < 6){
		ScrollBack(counter);
	}

	if (counter > 60 && scene == 0){
		scene = 1;
		Pattern1();
	}

	if (scene == 1){
		//if (counter / 10 % 4 == 0){
			scroll_b += 20;
		//}
		if (scroll_b >= 960){
			scroll_b = 960;
			scene = 2;
			b[3]->start = true;
		}
	}

	if (scene == 2 && toLeft(3, 1) && counter > 300){
		scene = 3;
	}

	if (scene == 3){
		//if (counter / 10 % 4 == 0){
			scroll_b += 20;
		//}
		if (scroll_b >= 1920){
			scroll_b = 1920;
			scene = 4;
			Pattern2();
		}
	}

	if (scene == 4 && toLeft(0, 10) && counter > 500){
		scene = 5;
	}

	if (scene == 5){
		scroll_b += 20;
		if (scroll_b >= 2880){
			scroll_b = 2880;
			scene = 6;
			Pattern3();
		}
	}
	if (scene == 6 && toLeft(0, 10)){
		scene = 7;
	}

	if (scene == 7 && counter > 700){
		doorX += 3;
		if (doorX >= 307){
			doorX = 307;
			scene = 8;
			mTime = 0;
		}
	}
	
	if(scene < 7) ManScroll(counter);
	else {
		if (scene == 7){
			if (mTime < 10){
				ManScroll(counter);
			}
			else if (mTime == 10){
				mNum = 3;
			}
			else if (mTime == 20){
				mPosY -= 100;
				mNum = 4;
			}
			else if (mTime == 25){
				mPosY -= 150;
				mNum = 5;
			}
		}
		else if (scene == 8){
			switch (mTime / 10 % 4){
			case 0:
				mNum = 5;
				break;
			case 1:
				mNum = 6;
				break;
			case 2:
				mNum = 5;
				break;
			case 3:
				mNum = 7;
				break;
			}
		}
		mTime++;
	}
	if (counter == 890)e2->useOut = true;
	Draw();
}

void Scene5::Draw(){
	if (e2->useIn == true) e2->fadeIn();
	if (e2->useOut == true && e2->fadeOut()){
		end = true;
	}
	if(scene >= 6) DrawRotaGraph2(0, 0, 0, 0, 1.0, 0, back, true, false);
	//DrawRotaGraph2(0, scroll2, 0, 0, 1.0, 0, back, true, false);
	if (scene < 6){
		DrawRectGraph(0, 0, 0, 960 - scroll1, 1280, scroll1, back, false, false);
		DrawRectGraph(0, scroll1, 0, 0, 1280, 960 - scroll1, back, false, false);
	}
	if(scene >= 3 && scene < 6) DrawRotaGraph2(0, scroll_b - 1920 + 20, 0, 0, 1.0, 0, piano, true, false);
	if (scene >= 5){
		DrawRotaGraph2(30, 20 + scroll_b - 960 * 3, 0, 0, 1.0, 0, speaker, true, false);
		DrawRotaGraph2(900, 20 + scroll_b - 960 * 3, 0, 0, 1.0, 0, speaker, true, false);
		DrawRotaGraph2(15, 560 + scroll_b - 960 * 3, 0, 0, 1.0, 0, key, true, false);
	}
	for (int i = 0; i < bNum; i++){
		if (b[i]->start && - 550 < b[i]->y + scroll_b && b[i]->y + scroll_b < 960){
			if (scene < 6){
				DrawRotaGraph2(b[i]->x, b[i]->y + scroll_b, 0, 0, 1.0, 0, block[b[i]->num], true, false);
				if (scene < 4){
					if (i < 3) DrawRotaGraph2(b[i]->x + 60, b[i]->y + scroll_b + 50, 0, 0, 1.0, 0, s_large[b[i]->ix], true, false);
					else DrawRotaGraph2(b[i]->x + 35, b[i]->y + scroll_b + 10, 0, 0, 1.0, 0, s_small[b[i]->ix], true, false);
				}
			}
			else{
				if(i < 27)
					DrawRotaGraph2(b[i]->x, b[i]->y + scroll_b, 0, 0, 1.0, 0, key_i[i], true, false);
				else DrawRotaGraph2(b[i]->x, b[i]->y + scroll_b, 0, 0, 1.0, 0, exit[i-27], true, false);
			}
		}
	}

	if (scene >= 7){
		DrawRectGraph(786 - doorX, 106, 308 - doorX, 0, doorX, 358, e_door, false, false);
	}

	if(scene < 8) DrawRotaGraph2(mPosX, mPosY + scroll_b + scroll2, 0, 0, 1.0, 0, man[mNum], true, false);
	if (scene == 8){
		DrawExtendGraph(mPosX + mTime / 6, mPosY + mTime / 3 + scroll_b + scroll2, mPosX + 173 - mTime / 6, mPosY + 351 - mTime / 3 + scroll_b + scroll2, man[mNum], true);
	}
}

void Scene5::ManScroll(int counter){
	mPosY -= 5;
	if (counter % 5 == 0){
		switch (counter % 20){
		case 0:
			mNum = 0;
			break;
		case 5:
			mNum = 1;
			break;
		case 10:
			mNum = 2;
			break;
		case 15:
			mNum = 0;
			break;
		}
	}
}

void Scene5::Pattern1(){
	bNum = 12;
	scroll_b = 0;
	
	for (int i = 0; i < 3; i++){
		b[i] = new Block(40, 15 + i * 310 - 960, 1, i, 40, true);// M T D
	}

	b[8] = new Block(1280, 780 - 960, 0, 5, 385, false);// u
	b[9] = new Block(1280, 780 - 960, 0, 1, 555, false);// s
	b[10] = new Block(1280, 780 - 960, 0, 6, 725, false);// i
	b[11] = new Block(1280, 780 - 960, 0, 7, 895, false);// c

	b[6] = new Block(1280, 460 - 960, 0, 3, 385, false);// o
	b[7] = new Block(1280, 470 - 960, 0, 4, 555, false);// p

	b[3] = new Block(1280, 140 - 960, 0, 0, 385, false);// e
	b[4] = new Block(1280, 150 - 960, 0, 1, 555, false);// s
	b[5] = new Block(1280, 140 - 960, 0, 2, 725, false);// k
}

void Scene5::Pattern2(){
	for (int i = 0; i < bNum; i++){
		delete b[i];
		b[i] = NULL;
	}
	bNum = 18;

	b[0] = new Block(1280, 35 - 960 * 2, 2, 0, 315, false);
	b[1] = new Block(1280, 35 + 66 * 1 - 960 * 2, 2, 0, 315 + 150, false);
	b[2] = new Block(1280, 35 + 66 * 2 - 960 * 2, 2, 0, 315 + 105 * 2, false);
	b[3] = new Block(1280, 35 + 66 * 3 - 960 * 2, 2, 0, 315 + 105 * 3, false);
	b[4] = new Block(1280, 35 + 66 * 4 - 960 * 2, 2, 0, 315 + 105 * 4, false);
	b[5] = new Block(1280, 35 + 66 * 3 - 960 * 2, 2, 0, 315 + 105 * 5, false);
	b[6] = new Block(1280, 35 + 66 * 3 - 960 * 2, 2, 0, 315 + 105 * 6, false);
	b[7] = new Block(1280, 35 + 66 * 4 - 960 * 2, 2, 0, 315 + 105 * 7, false);
	b[8] = new Block(1280, 35 + 66 * 3 - 960 * 2, 2, 0, 315 + 105 * 8, false);
	b[9] = new Block(1280, 35 + 66 * 8 - 960 * 2, 2, 0, 315, false);
	b[10] = new Block(1280, 35 + 66 * 11 - 960 * 2, 2, 0, 315 + 105 , false);
	b[11] = new Block(1280, 35 + 66 * 10 - 960 * 2, 2, 0, 315 + 105 * 2, false);
	b[12] = new Block(1280, 35 + 66 * 9  - 960 * 2, 2, 0, 315 + 105 * 3, false);
	b[13] = new Block(1280, 35 + 66 * 12 - 960 * 2, 2, 0, 315 + 105 * 4, false);
	b[14] = new Block(1280, 35 + 66 * 12 - 960 * 2, 2, 0, 315 + 105 * 5, false);
	b[15] = new Block(1280, 35 + 66 * 12 - 960 * 2, 2, 0, 315 + 105 * 6, false);
	b[16] = new Block(1280, 35 + 66 * 11 - 960 * 2, 2, 0, 315 + 105 * 7, false);
	b[17] = new Block(1280, 35 + 66 * 8 - 960 * 2, 2, 0, 315 + 105 * 8, false);

	b[0]->start = true;
}

void Scene5::Pattern3(){
	for (int i = 0; i < bNum; i++){
		delete b[i];
		b[i] = NULL;
	}
	bNum = 31;

	for (int i = 0; i < bNum - 4; i++){
		b[i] = new Block(1280, 563 + i / 9 * 107 - 960 * 3, 2, 0, 18 + i % 9 * 139, false);
	}


	b[27] = new Block(1280, 40 - 960 * 3, 2, 0, 411, false);
	b[28] = new Block(1280, 40 - 960 * 3, 2, 0, 631, false);
	b[29] = new Block(1280, 285 - 960 * 3, 2, 0, 411, false);
	b[30] = new Block(1280, 285 - 960 * 3, 2, 0, 631, false);

	b[0]->start = true;
}

bool Scene5::toLeft(int_fast8_t startNum, int_fast8_t speed){
	//最後のブロックが落ちてたらtrue
	if (b[bNum-1]->x == b[bNum-1]->dx) return true;

	for (int_fast8_t i = startNum; i < bNum; i++){
		if (b[i]->start){
			b[i]->time++;

			if (b[i]->fell == false){
				b[i]->x -= 12 * b[i]->time * b[i]->time * 0.01 * speed;

				if (b[i]->x <= b[i]->dx){
					b[i]->x = b[i]->dx;
					b[i]->fell = true;
				}
			}
			if (b[i]->time == 15 - speed && i != bNum-1){
				b[i + 1]->start = true;
			}
		}

	}

	return false;
}

void Scene5::ScrollBack(int count){
	//if (count / 10 % 4 == 0){
		scroll1 += 20;
		if(scene == 0) scroll2 += 20;
	//}

	if (scroll1 >= 960){
		//scroll1 -= 1920;
		scroll1 -= 960;
	}
}

Scene5::~Scene5(){
	delete e2;
	e2 = NULL;
}
