#ifndef SCENE2_B_INCLUDED__
#define SCENE2_B_INCLUDED__
#include "effect.h"
#include <cstdint>

class Scene2{
	int_fast8_t scene{ 0 };
	int mark{ 0 };

	//背景に関する変数
	int_fast16_t backX{ 0 };
	int back{ 0 };

	//人に関する変数
	static const int_fast8_t manNum{ 11 };
	int man[manNum];
	int_fast8_t mNum{ 0 };
	int_fast16_t mPosX{ 0 };
	int_fast16_t mPosY{ 0 };

	//剣に関する変数
	int sword[5];
	int_fast8_t sNum{ 0 };
	int_fast16_t sPosX{ 0 };
	int_fast16_t sPosY{ 0 };

	//TV画面に関する変数
	int movieHandle;
	int subjectHandle[2];
	int_fast8_t subjectNum{ 0 };
	int binetHandle;
	int crashHandle;
	int dodaiHandle;
	int_fast16_t tvX{ 0 };
	int_fast16_t tvY{ 0 };


	Effect2* e2;
	Effect3* e3;
	Effect4* e4;
	void Draw();
public:
	Scene2();
	~Scene2();
	bool end{ false };
	void Update(int counter);
	void Start(){ e2->useIn = true; }
};

class Mushroom{
public:
	Mushroom(int x, int y, int firstY);
	void Pattern();
	void Draw(int screenX);
	int picture[3];
	int_fast16_t x{ 0 };
	int_fast16_t y{ 0 };
	int_fast16_t difference{ 0 };
	float time{ 0 };
	bool stop{ false };
	bool dead{ false };
	bool end{ false };
private:
	int_fast16_t firstY{ 0 };
};

#endif