#include"DxLib.h"
#include"scene4.h"
#include <string>

Scene4::Scene4(){
	lengthW = 1220 / Box::numW;
	lengthH = 960 / Box::numH;
	for (int_fast8_t i = 0; i < Box::numW; i++){
		for (int_fast8_t j = 0; j < Box::numH; j++){
			box[i][j] = new Box(i*lengthW, -100, i*lengthW, (Box::numH - j - 1)*lengthH);
		}
		showTop[i] = false;
	}
	box[0][0]->start = true;
	exitDoor = LoadGraph("img/scene4/exit.png");
	door = LoadGraph("img/scene4/door.png");
	doorEdge = LoadGraph("img/scene4/doorEdge.png");
	secret = LoadGraph("img/scene4/secret.png");
	button = LoadGraph("img/scene4/button.png");
	boxSide = LoadGraph("img/scene4/box.png");
	boxTop = LoadGraph("img/scene4/top.png");
	back = LoadGraph("img/scene4/back2.png");
	LoadDivGraph("img/scene4/web2.png", Box::numW*Box::numH, Box::numW, Box::numH, lengthW, lengthH, picture[0]);
	LoadDivGraph("img/scene4/web.png", Box::numW*Box::numH, Box::numW, Box::numH, lengthW, lengthH, picture[1]);
	LoadDivGraph("img/scene4/web1.png", Box::numW*Box::numH, Box::numW, Box::numH, lengthW, lengthH, picture[2]);
	for (int i = 0; i < manNum; i++){
		man[i] = LoadGraph(("img/scene4/man/" + std::to_string(i) + ".png").c_str());
	}

	mPosX = -260;
	mPosY = 540;
	//最初に画面を暗くする
	//SetDrawBright(0, 0, 0);
	e2 = new Effect2;
	//e2->useIn = true;
}

void Scene4::Update(int counter){
	if (scene == 0){
		mPosX += 10;
		mNum = counter / 5 % 3;

		if (counter > 130){
			dX += 4;
			if (dX > 110){
				dX = 110;
			}
		}
	}

	if (scene == 0 && counter > 60 && Fall(960)){
		scene = 1;
		for (int_fast8_t i = 0; i < Box::numW; i++){
			showTop[i] = true;
		}
	}
	
	if (scene == 1 && time++ >= 60){
		scrollY += 40;
		topY += 40;
		if (scrollY >= 960){
			scene = 2;
			scrollY = 0;
			topY = 960;
			time = 0;
			
			pictureNum++;
			ReadyForFall();
		}
	}
	if (scene == 2 && Fall(960)){
		if (time >= 60){
			scrollY += 40;
			topY += 40;
			if (scrollY >= 960){
				scene = 3;
				scrollY = 0;
				topY = 960;
				time = 0;

				pictureNum++;
				if (pictureNum == 3)pictureNum = 0;
				ReadyForFall();
			}
		}
		else {
			if (time == 0){
				topY = 0;
				for (int_fast8_t i = 0; i < Box::numW; i++){
					showTop[i] = true;
				}
			}
			time++;
		}
	}

	if (scene == 3 && Fall(960)){
		time++;
		if (time >= 60){
			scene = 4;
		}
	}

	if (scene == 4){
		scrollX -= 40;
		if (scrollX <= -1280){
			scrollX = -1280;
			scene = 5;
			mNum = 4;
			mPosX = 710;
			mPosY = 60;
		}
	}

	if (scene == 5){
		dX -= 4;
		if (dX <= 0){
			dX = 0;
		}
		if (dX < 55){
			mPosX += 4;
			mNum = counter / 5 % 3 + 3;
		}
		if (mPosX > 1100) e2->useOut = true;
	}
	
	Draw();
}

void Scene4::Draw(){
	if (e2->useIn == true) e2->fadeIn();
	if (e2->useOut == true && e2->fadeOut()){
		end = true;
	}

	DrawGraph(0 + scrollX, 0, back, false);
	if (scene == 0){
		DrawRectGraph(1054, 494, 0, 0, dX, 424 + scrollY, door, true, false);
		DrawRotaGraph2(1054 + dX, 493 + dX / 2 + scrollY, 0, 0, 1.0, 0, doorEdge, true, false);
	}
	if (scene >= 4){
		DrawRotaGraph2(1280 * 2 - 441 + scrollX, 0, 0, 0, 1.0, 0, exitDoor, true, false);
		DrawRectGraph(1280 * 2 - 388 + scrollX + 110 - dX, 26, 0, 0, dX, 424, door, true, true);
		DrawRotaGraph2(1280 * 2 - 285 + scrollX - dX, 20 + dX / 2, 0, 0, 1.0, 0, doorEdge, true, true);
	}
	DrawRotaGraph2(mPosX, mPosY, 0, 0, 1.0, 0, man[mNum], true, false);
	if (scene >= 4){
		DrawFillBox(710, 60, 810, 260, GetColor(0, 0, 0));
	}

	if (scene != 0 && scene < 4) DrawRotaGraph2(1044, 479, 0, 0, 1.0, 0, secret, false, false);
	if (scene == 0) DrawRotaGraph2(1163, 543 + scrollY, 0, 0, 1.0, 0, button, false, false);
	if (scene >= 4) DrawRotaGraph2(2059 + scrollX, 73, 0, 0, 1.0, 0, button, false, true);

	//ブロック
	for (int_fast8_t i = Box::numW - 1; i >= 0; i--){
		for (int_fast8_t j = Box::numH - 2; j >= 0; j--){
			if (box[i][j]->start){
				if (j != Box::numH - 1 && box[i][j + 1]->fell == false){
					DrawRotaGraph2(box[i][j]->x - 243, box[i][j]->y - 160 + scrollY, 0, 0, 1.0, 0, boxSide, true, false);
				}
			}
		}
		if (showTop[i]){
			DrawRotaGraph2(box[i][Box::numH - 1]->x - 243, topY - 160, 0, 0, 1.0, 0, boxTop, true, false);
		}
	}
	//ブロックの絵
	for (int_fast8_t i = 0; i < Box::numW; i++){
		for (int_fast8_t j = 0; j < Box::numH; j++){
			if (box[i][j]->start && box[i][j]->y < 960 && box[i][j]->x >= 0){
				DrawRotaGraph2(box[i][j]->x + scrollX, box[i][j]->y + scrollY, 0, 0, 1.0, 0, picture[pictureNum][(Box::numH - 1 - j)*Box::numW + i], true, false);
			}
		}
	}
}

Scene4::~Scene4(){
	delete e2;
	e2 = NULL;
	for (int_fast8_t i = 0; i < Box::numW; i++){
		for (int_fast8_t j = 0; j < Box::numH; j++){
			delete box[i][j];
			box[i][j] = NULL;
		}
	}

	//InitGraph();
}

Box::Box(int_fast16_t x, int_fast16_t y, int_fast16_t dx, int_fast16_t dy){
	this->x = x;
	this->y = y;
	this->dx = dx;
	this->dy = dy;
}

bool Scene4::Fall(int_fast16_t baseLine){
	//最後のブロックが落ちてたらtrue
	if (box[Box::numW-1][Box::numH-1]->y == 0) return true;

	for (int_fast8_t i = 0; i < Box::numW; i++){
		for (int_fast8_t j = 0; j < Box::numH; j++){
			if (box[i][j]->start){
				box[i][j]->time++;

				if (box[i][j]->fell == false){
					box[i][j]->y += 6 *  box[i][j]->time * box[i][j]->time * 0.01;

					if (box[i][j]->y >= baseLine - (j + 1)*lengthH){
						box[i][j]->y = baseLine - (j + 1)*lengthH;
						box[i][j]->fell = true;
					}
				}
				if (box[i][j]->time == 15){
					box[i][(j + 1) % Box::numH]->start = true;
				}
				if (box[i][j]->time == 5 && i + 1 != Box::numW && j == 0){
					box[i+1][0]->start = true;
				}
			}
		}
		if (box[i][0]->fell) {
			showTop[i] = false;
		}
	}

	return false;
}

void Scene4::ReadyForFall(){
	for (int_fast8_t i = 0; i < Box::numW; i++){
		for (int_fast8_t j = 0; j < Box::numH; j++){
			box[i][j]->y = -100;
			box[i][j]->fell = false;
			box[i][j]->start = false;
			box[i][j]->time = 0;
		}
	}
	box[0][0]->start = true;
}