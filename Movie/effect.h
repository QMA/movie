#ifndef EFFECT_INCLUDED__
#define EFFECT_INCLUDED__

/* op斬撃 */
class Effect0{
	static const int pictureNum{ 5 };
	int picture[pictureNum];
	int showNum{ 0 };
	int count{ 0 };
	int alpha{ 255 };
	void Update();
public:
	bool use{ false };
	void Draw(int x, int y);
	Effect0();
};

/* 斬られたとき用 */
class Effect1{
	int count{ 0 };
	int move{ 0 };
public:
	bool use{ false };
	int Move(const int value);
};

/* fadeIn or fadeOut */
class Effect2{
	int countIn{ 0 };
	int countOut{ 0 };
	int countOutSemi{ 0 };
public:
	bool useIn{ false };
	bool useOut{ false };
	bool useOutSemi{ false };
	bool fadeIn();
	bool fadeOut();
	bool fadeOutSemi();
};

/* op斬撃のでかい版 */
class Effect3{
	static const int pictureNum{ 5 };
	int picture[pictureNum];
	int showNum{ 0 };
	int count{ 0 };
	int alpha{ 255 };
	void Update();
public:
	bool use{ false };
	void Init();
	void Draw(int x, int y);
	Effect3();
};

class Effect4{
	int* picture;
	int showNum{ 0 };
	int count{ 0 };
public:
	bool use{ false };
	Effect4(int* picture){ this->picture = picture; };
	void Draw(int x, int y);
	void Update();
};
#endif