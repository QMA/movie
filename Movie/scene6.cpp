#include "scene6.h"
#include "DxLib.h"
#include <string>

Scene6::Scene6(){
	white = LoadGraph("img/scene6/white.png");
	sword = LoadGraph("img/scene6/sword_3.png");
	back_u = LoadGraph("img/scene6/back_u.png");
	back_d = LoadGraph("img/scene6/back_d.png");
	back_alpha = LoadGraph("img/scene6/back_alpha.png");
	san = LoadGraph("img/scene6/san.png");
	mark = LoadGraph("img/scene6/mark_3.png");
	black = LoadGraph("img/scene6/mask.png");

	address = LoadGraph("img/scene6/address.png");
	syoriken = LoadGraph("img/scene6/syoriken.png");
	jouhou = LoadGraph("img/scene6/jouhou.png");

	END[0] = LoadGraph("img/scene6/E.png");
	END[1] = LoadGraph("img/scene6/N.png");
	END[2] = LoadGraph("img/scene6/D.png");
	
	for (int i = 0; i < manNum; i++){
		man[i] = LoadGraph(("img/scene6/man/" + std::to_string(i) + ".png").c_str());
	}
	/*for (int i = 0; i < 2; i++){
		mark[i] = LoadGraph(("img/scene6/mark" + std::to_string(i) + ".png").c_str());
	}*/

	e2 = new Effect2;
	//e2->useIn = true;
	//SetDrawBright(0, 0, 0);
}

void Scene6::Update(int counter){
	if (counter > 60 && scene < 3 && alpha < 255){
		alpha += 1.2f;

		if (alpha >= 255){
			alpha = 0.0f;
			scene = 3;
		}
	}
	else if (scene == 3 && alpha < 255 && counter > 310){
		alpha += 5.2f;

		if (alpha >= 255){
			alpha = 0.0f;
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			scene = 4;
		}
	}

	if (counter == 1){
		scene = 1;
	}
	if (scene == 1){
		mPosY -= 2;

		switch (counter / 10 % 4){
		case 0:
			mNum = 0;
			break;
		case 1:
			mNum = 1;
			break;
		case 2:
			mNum = 0;
			break;
		case 3:
			mNum = 2;
			break;
		}

		if (mPosY <= 590){
			mPosY = 590;
			mNum = 0;
			scene = 2;
		}
	}
	else if (scene == 2 && alpha >= 200){
		mNum = 3;
	}

	else if (scene == 4){
		//alpha -= 25.2f;

		//if (alpha <= 0){
		//	alpha = 0.0f;
		//	//scene = 5;
		//}

		angle -= 0.09;
		if (angle <= -3.14){
			angle = -3.14;
			scene = 5;
		}
	}
	else if (scene == 5){
		alpha += 10.2f;

		if (alpha >= 255.0){
			alpha = 0.0f;
			scene = 6;
		}
	}
	else if (scene == 6){
		alpha += 10.2f;

		if (alpha >= 255.0){
			alpha = 0.0f;
			scene = 7;
		}
	}
	else if (scene == 7){
		alpha += 10.2f;

		if (alpha >= 255.0){
			alpha = 255.0f;
			if (counter > 550){
				scene = 8;
				alpha = 0.0f;
			}
		}
	}
	else if (scene == 8){
		alpha += 10.2f;

		if (alpha >= 255.0){
			alpha = 0.0f;
			scene = 9;
		}
	}
	else if (scene == 9){
		alpha += 10.2f;

		if (alpha >= 255.0 ){
			alpha = 255.0f;
			if (counter > 660){
				alpha = 0.0f;
				scene = 10;
			}
		}
	}
	else if (scene == 10){
		alpha += 10.2f;

		if (alpha >= 255.0){
			//end = true;
		}
	}

	Draw();
}

void Scene6::Draw(){
	if (e2->useIn == true) e2->fadeIn();
	/*if (e2->useOut == true && e2->fadeOut()){
		end = true;
	}*/
	if (scene < 4){
		//�w�i�Q
		if (scene == 3)SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - alpha);
		DrawRotaGraph2(0, 0, 0, 0, 1.0, 0, back_u, true, false);

		if (scene < 3) DrawRotaGraph2(0, 765 - (int)(alpha * 3), 0, 0, 1.0, 0, san, true, false);
		else if (scene == 3){
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
			DrawRotaGraph2(0, 0, 0, 0, 1.0, 0, san, true, false);
			//SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
			//DrawRotaGraph2(510, 340, 0, 0, 1.0, 0, black, true, false);
		}

		if (scene == 3)SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - alpha);
		DrawRotaGraph2(0, 542, 0, 0, 1.0, 0, back_d, true, false);

		//���ɂ�����
		if (scene < 3) SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
		else if (scene == 3)SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - alpha);
		DrawRotaGraph2(0, 0, 0, 0, 1.0, 0, back_alpha, true, false);

		//���̕`��
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
		if (mNum == 3) DrawRotaGraph2(590, 440, 0, 0, 1.0, 0, sword, true, false);
		//590x440, mPosX + 60, mPosY - 180
		if (scene == 3){
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
			DrawRotaGraph2(0, 0, 0, 0, 1.0, 0, black, true, false);
		}

		//�l�̕`��
		if (scene == 3)SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - alpha);
		DrawRotaGraph2(mPosX, mPosY, 0, 0, 1.0, 0, man[mNum], true, false);
	}
	else {
		//if (scene == 4){
		SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255);
		if(scene < 8)DrawRotaGraph(453 + (int)(angle * 70) + 181, 290 + 181, 1.0 + 0.125 * angle, angle, mark, true, false);
		if (scene == 5){
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
			DrawRotaGraph2(60, 314 + 25 - alpha / 10, 0, 0, 1.0, 0, syoriken, true, false);
		}
		else if (scene == 6){
			DrawRotaGraph2(60, 314, 0, 0, 1.0, 0, syoriken, true, false);

			SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
			DrawRotaGraph2(70 + 25 - alpha/10, 170, 0, 0, 1.0, 0, jouhou, true, false);
		}
		else if (scene == 7){
			DrawRotaGraph2(60, 314, 0, 0, 1.0, 0, syoriken, true, false);
			DrawRotaGraph2(70, 170, 0, 0, 1.0, 0, jouhou, true, false);

			SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
			DrawRotaGraph2(510 - 25 + alpha / 10, 720, 0, 0, 1.0, 0, address, true, false);
		}
		else if (scene == 8){
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - alpha);
			DrawRotaGraph(415, 471 - alpha / 10, 0.6075, 3.14, mark, true, false);
			DrawRotaGraph2(60, 314 - alpha / 10, 0, 0, 1.0, 0, syoriken, true, false);
			DrawRotaGraph2(70 - alpha / 10, 170, 0, 0, 1.0, 0, jouhou, true, false);
			DrawRotaGraph2(510 + alpha / 10, 720, 0, 0, 1.0, 0, address, true, false);
		}
		else if (scene == 9){
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, alpha);
			DrawRotaGraph2(280 + 55 - alpha / 5, 337, 0, 0, 1.0, 0, END[0], true, false);
			DrawRotaGraph2(527, 337 - 55 + alpha / 5, 0, 0, 1.0, 0, END[1], true, false);
			DrawRotaGraph2(865, 335 + 55 - alpha / 5, 0, 0, 1.0, 0, END[2], true, false);
		}
		else if (scene == 10){
			SetDrawBlendMode(DX_BLENDMODE_ALPHA, 255 - alpha);
			DrawRotaGraph2(280 - alpha / 5, 337, 0, 0, 1.0, 0, END[0], true, false);
			DrawRotaGraph2(527, 337 + alpha / 5, 0, 0, 1.0, 0, END[1], true, false);
			DrawRotaGraph2(865, 335 - alpha / 5, 0, 0, 1.0, 0, END[2], true, false);
		}
	}
}

Scene6::~Scene6(){

}